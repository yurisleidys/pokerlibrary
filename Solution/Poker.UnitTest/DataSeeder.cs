﻿using Poker.Logic.Models;

namespace Poker.UnitTest
{
    public static class DataSeeder
    {
        public static Player PlayerJoe_AllHeartsSuit_KingHigherRank()
        {
            return new Player
            {
                Name = "Joe",
                Cards = new Card[]
                {
                    new Card { Suit = Suit.Hearts, Rank = Rank.Two },
                    new Card { Suit = Suit.Hearts, Rank = Rank.Jack },
                    new Card { Suit = Suit.Hearts, Rank = Rank.Six },
                    new Card { Suit = Suit.Hearts, Rank = Rank.Eight },
                    new Card { Suit = Suit.Hearts, Rank = Rank.King }
                }
            };
        }

        public static Player PlayerRick_AllDiamondsSuit_SameRankPlayerJoe()
        {
            return new Player
            {
                Name = "Rick",
                Cards = new Card[]
                {
                    new Card { Suit = Suit.Diamonds, Rank = Rank.Two },
                    new Card { Suit = Suit.Diamonds, Rank = Rank.Jack },
                    new Card { Suit = Suit.Diamonds, Rank = Rank.Six },
                    new Card { Suit = Suit.Diamonds, Rank = Rank.Eight },
                    new Card { Suit = Suit.Diamonds, Rank = Rank.King }
                }
            };
        }

        public static Player PlayerJason_AllClubsSuit_AceHigherRank()
        {
            return new Player
            {
                Name = "Jason",
                Cards = new Card[]
                {
                    new Card { Suit = Suit.Clubs, Rank = Rank.Two },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Jack },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Five },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Eight },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Ace }
                }
            };
        }

        public static Player PlayerJen_3ClubsSuit_AceHigherRank()
        {
            return new Player
            {
                Name = "Jen",
                Cards = new Card[]
                {
                    new Card { Suit = Suit.Hearts, Rank = Rank.Two },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Five },
                    new Card { Suit = Suit.Spades, Rank = Rank.Seven },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Ten },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Ace }
                }
            };
        }

        public static Player PlayerPeter_2Clubs2DiamondsSuit_3ThreeRank()
        {
            return new Player
            {
                Name = "Peter",
                Cards = new Card[]
                {
                    new Card { Suit = Suit.Clubs, Rank = Rank.Three },
                    new Card { Suit = Suit.Diamonds, Rank = Rank.Three },
                    new Card { Suit = Suit.Spades, Rank = Rank.Three },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Eight },
                    new Card { Suit = Suit.Diamonds, Rank = Rank.Ten }
                }
            };
        }

        public static Player PlayerMary_2ClubsSuit_3TenRank()
        {
            return new Player
            {
                Name = "Mary",
                Cards = new Card[]
                {
                    new Card { Suit = Suit.Hearts, Rank = Rank.King },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Ten },
                    new Card { Suit = Suit.Spades, Rank = Rank.Jack },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Ten },
                    new Card { Suit = Suit.Diamonds, Rank = Rank.Ten }
                }
            };
        }

        public static Player PlayerDon_3SpadesSuit_3TenRank()
        {
            return new Player
            {
                Name = "Don",
                Cards = new Card[]
                {
                    new Card { Suit = Suit.Spades, Rank = Rank.King },
                    new Card { Suit = Suit.Spades, Rank = Rank.Ten },
                    new Card { Suit = Suit.Spades, Rank = Rank.Queen },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Ten },
                    new Card { Suit = Suit.Diamonds, Rank = Rank.Ten }
                }
            };
        }

        public static Player PlayerJack_2DiamondsSuit_2EightRank()
        {
            return new Player
            {
                Name = "Jack",
                Cards = new Card[]
                {
                    new Card { Suit = Suit.Diamonds, Rank = Rank.Two },
                    new Card { Suit = Suit.Diamonds, Rank = Rank.King },
                    new Card { Suit = Suit.Spades, Rank = Rank.Eight },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Eight },
                    new Card { Suit = Suit.Hearts, Rank = Rank.Six }
                }
            };
        }

        public static Player PlayerEly_2HeartsSuit_2NineRank()
        {
            return new Player
            {
                Name = "Ely",
                Cards = new Card[]
                {
                    new Card { Suit = Suit.Hearts, Rank = Rank.Ace },
                    new Card { Suit = Suit.Diamonds, Rank = Rank.Jack },
                    new Card { Suit = Suit.Spades, Rank = Rank.Nine },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Nine },
                    new Card { Suit = Suit.Hearts, Rank = Rank.Five }
                }
            };
        }

        public static Player PlayerMia_2ClubsSuit_2NineRank()
        {
            return new Player
            {
                Name = "Mia",
                Cards = new Card[]
                {
                    new Card { Suit = Suit.Hearts, Rank = Rank.Four },
                    new Card { Suit = Suit.Diamonds, Rank = Rank.King },
                    new Card { Suit = Suit.Spades, Rank = Rank.Nine },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Nine },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Five }
                }
            };
        }

        public static Player PlayerPaul_2SpadesSuit_JackHigherRank()
        {
            return new Player
            {
                Name = "Paul",
                Cards = new Card[]
                {
                    new Card { Suit = Suit.Spades, Rank = Rank.Two },
                    new Card { Suit = Suit.Diamonds, Rank = Rank.Jack },
                    new Card { Suit = Suit.Spades, Rank = Rank.Three },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Eight },
                    new Card { Suit = Suit.Hearts, Rank = Rank.Six }
                }
            };
        }

        public static Player PlayerJosh_3HeartsSuit_QueenHigherRank()
        {
            return new Player
            {
                Name = "Josh",
                Cards = new Card[]
                {
                    new Card { Suit = Suit.Hearts, Rank = Rank.Ten },
                    new Card { Suit = Suit.Hearts, Rank = Rank.Queen },
                    new Card { Suit = Suit.Spades, Rank = Rank.Two },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Seven },
                    new Card { Suit = Suit.Hearts, Rank = Rank.Jack }
                }
            };
        }

        public static Player PlayerTed_3DiamondsSuit_QueenHigherRank()
        {
            return new Player
            {
                Name = "Ted",
                Cards = new Card[]
                {
                    new Card { Suit = Suit.Diamonds, Rank = Rank.Ten },
                    new Card { Suit = Suit.Hearts, Rank = Rank.Queen },
                    new Card { Suit = Suit.Diamonds, Rank = Rank.Three },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Jack },
                    new Card { Suit = Suit.Diamonds, Rank = Rank.Seven }
                }
            };
        }

        public static Player PlayerRay_2SpadesSuit_QueenHigherRank()
        {
            return new Player
            {
                Name = "Ray",
                Cards = new Card[]
                {
                    new Card { Suit = Suit.Spades, Rank = Rank.Queen },
                    new Card { Suit = Suit.Hearts, Rank = Rank.Seven },
                    new Card { Suit = Suit.Diamonds, Rank = Rank.Jack },
                    new Card { Suit = Suit.Clubs, Rank = Rank.Three },
                    new Card { Suit = Suit.Spades, Rank = Rank.Ten }
                }
            };
        }
    }
}
