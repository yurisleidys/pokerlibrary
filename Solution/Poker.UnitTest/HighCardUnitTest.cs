using Poker.Logic.Interfaces;
using Poker.Logic.Models;
using Poker.Logic.Services;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Poker.UnitTest
{
    public class HighCardUnitTest
    {
        private readonly IHandEvaluationService _handEvaluationService;

        public HighCardUnitTest()
        {
            _handEvaluationService = new HandEvaluationService();
        }

        [Fact]
        public void One_Player_Has_HighCard()
        {
            // Arrange
            var players = new List<Player>
            {
                DataSeeder.PlayerPaul_2SpadesSuit_JackHigherRank(),
                DataSeeder.PlayerJen_3ClubsSuit_AceHigherRank(),
                DataSeeder.PlayerJosh_3HeartsSuit_QueenHigherRank()
            };

            // Action
            var winners = _handEvaluationService.GetWinners(players);

            // Assert
            Assert.Single(winners);
            Assert.Equal("Jen", winners.First().Name);
        }

        [Fact]
        public void Two_Players_Have_HighCard_Different_Rank()
        {
            // Arrange
            var players = new List<Player>
            {
                DataSeeder.PlayerPaul_2SpadesSuit_JackHigherRank(),
                DataSeeder.PlayerJosh_3HeartsSuit_QueenHigherRank(),
                DataSeeder.PlayerTed_3DiamondsSuit_QueenHigherRank(),
            };

            // Action
            var winners = _handEvaluationService.GetWinners(players);

            // Assert
            Assert.Single(winners);
            Assert.Equal("Ted", winners.First().Name);
        }

        [Fact]
        public void Two_Players_Have_HighCard_Same_Rank()
        {
            // Arrange
            var players = new List<Player>
            {
                DataSeeder.PlayerPaul_2SpadesSuit_JackHigherRank(),
                DataSeeder.PlayerJosh_3HeartsSuit_QueenHigherRank(),
                DataSeeder.PlayerTed_3DiamondsSuit_QueenHigherRank(),
                DataSeeder.PlayerRay_2SpadesSuit_QueenHigherRank()
            };

            // Action
            var winners = _handEvaluationService.GetWinners(players);

            // Assert
            Assert.True(winners.Count == 2);
            Assert.Equal("Ted", winners.First().Name);
            Assert.Equal("Ray", winners.Last().Name);
        }
    }
}
