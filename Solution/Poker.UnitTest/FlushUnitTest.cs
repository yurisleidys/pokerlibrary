using Poker.Logic.Interfaces;
using Poker.Logic.Models;
using Poker.Logic.Services;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Poker.UnitTest
{
    public class FlushUnitTest
    {
        private readonly IHandEvaluationService _handEvaluationService;

        public FlushUnitTest()
        {
            _handEvaluationService = new HandEvaluationService();
        }

        [Fact]
        public void One_Player_Has_Flush()
        {
            // Arrange
            var players = new List<Player>
            {
                DataSeeder.PlayerJoe_AllHeartsSuit_KingHigherRank(),
                DataSeeder.PlayerJen_3ClubsSuit_AceHigherRank(),
                DataSeeder.PlayerPeter_2Clubs2DiamondsSuit_3ThreeRank()
            };

            // Action
            var winners = _handEvaluationService.GetWinners(players);

            // Assert
            Assert.Single(winners);
            Assert.Equal("Joe", winners.First().Name);
        }

        [Fact]
        public void Two_Players_Have_Flush_Same_Rank()
        {
            // Arrange
            var players = new List<Player>
            {
                DataSeeder.PlayerJoe_AllHeartsSuit_KingHigherRank(),
                DataSeeder.PlayerRick_AllDiamondsSuit_SameRankPlayerJoe(),
                DataSeeder.PlayerJen_3ClubsSuit_AceHigherRank(),
                DataSeeder.PlayerPeter_2Clubs2DiamondsSuit_3ThreeRank()
            };

            // Action
            var winners = _handEvaluationService.GetWinners(players);

            // Assert
            Assert.True(winners.Count == 2);
            Assert.Equal("Joe", winners.First().Name);
            Assert.Equal("Rick", winners.Last().Name);
        }

        [Fact]
        public void Two_Players_Have_Flush_Different_Rank()
        {
            // Arrange
            var players = new List<Player>
            {
                DataSeeder.PlayerJoe_AllHeartsSuit_KingHigherRank(),
                DataSeeder.PlayerJason_AllClubsSuit_AceHigherRank(),
                DataSeeder.PlayerJen_3ClubsSuit_AceHigherRank(),
                DataSeeder.PlayerPeter_2Clubs2DiamondsSuit_3ThreeRank()
            };

            // Action
            var winners = _handEvaluationService.GetWinners(players);

            // Assert
            Assert.Single(winners);
            Assert.Equal("Jason", winners.First().Name);
        }
    }
}
