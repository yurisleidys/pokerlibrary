using Poker.Logic.Interfaces;
using Poker.Logic.Models;
using Poker.Logic.Services;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Poker.UnitTest
{
    public class OnePairUnitTest
    {
        private readonly IHandEvaluationService _handEvaluationService;

        public OnePairUnitTest()
        {
            _handEvaluationService = new HandEvaluationService();
        }

        [Fact]
        public void One_Player_Has_OnePair()
        {
            // Arrange
            var players = new List<Player>
            {
                DataSeeder.PlayerPaul_2SpadesSuit_JackHigherRank(),
                DataSeeder.PlayerJen_3ClubsSuit_AceHigherRank(),
                DataSeeder.PlayerJack_2DiamondsSuit_2EightRank()
            };

            // Action
            var winners = _handEvaluationService.GetWinners(players);

            // Assert
            Assert.Single(winners);
            Assert.Equal("Jack", winners.First().Name);
        }

        [Fact]
        public void Two_Players_Have_OnePair_Different_Rank()
        {
            // Arrange
            var players = new List<Player>
            {
                DataSeeder.PlayerPaul_2SpadesSuit_JackHigherRank(),
                DataSeeder.PlayerJen_3ClubsSuit_AceHigherRank(),
                DataSeeder.PlayerJack_2DiamondsSuit_2EightRank(),
                DataSeeder.PlayerEly_2HeartsSuit_2NineRank()
            };

            // Action
            var winners = _handEvaluationService.GetWinners(players);

            // Assert
            Assert.Single(winners);
            Assert.Equal("Ely", winners.First().Name);
        }

        [Fact]
        public void Two_Players_Have_OnePair_Same_Rank()
        {
            // Arrange
            var players = new List<Player>
            {
                DataSeeder.PlayerPaul_2SpadesSuit_JackHigherRank(),
                DataSeeder.PlayerJen_3ClubsSuit_AceHigherRank(),
                DataSeeder.PlayerJack_2DiamondsSuit_2EightRank(),
                DataSeeder.PlayerEly_2HeartsSuit_2NineRank(),
                DataSeeder.PlayerMia_2ClubsSuit_2NineRank()
            };

            // Action
            var winners = _handEvaluationService.GetWinners(players);

            // Assert
            Assert.Single(winners);
            Assert.Equal("Ely", winners.First().Name);
        }
    }
}
