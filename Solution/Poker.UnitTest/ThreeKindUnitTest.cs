using Poker.Logic.Interfaces;
using Poker.Logic.Models;
using Poker.Logic.Services;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Poker.UnitTest
{
    public class ThreeKindUnitTest
    {
        private readonly IHandEvaluationService _handEvaluationService;

        public ThreeKindUnitTest()
        {
            _handEvaluationService = new HandEvaluationService();
        }

        [Fact]
        public void One_Player_Has_ThreeKind()
        {
            // Arrange
            var players = new List<Player>
            {
                DataSeeder.PlayerJen_3ClubsSuit_AceHigherRank(),
                DataSeeder.PlayerPeter_2Clubs2DiamondsSuit_3ThreeRank(),
                DataSeeder.PlayerPaul_2SpadesSuit_JackHigherRank()
            };

            // Action
            var winners = _handEvaluationService.GetWinners(players);

            // Assert
            Assert.Single(winners);
            Assert.Equal("Peter", winners.First().Name);
        }

        [Fact]
        public void Two_Players_Have_ThreeKind_Different_Rank()
        {
            // Arrange
            var players = new List<Player>
            {
                DataSeeder.PlayerJen_3ClubsSuit_AceHigherRank(),
                DataSeeder.PlayerPeter_2Clubs2DiamondsSuit_3ThreeRank(),
                DataSeeder.PlayerMary_2ClubsSuit_3TenRank(),
                DataSeeder.PlayerPaul_2SpadesSuit_JackHigherRank()
            };

            // Action
            var winners = _handEvaluationService.GetWinners(players);

            // Assert
            Assert.Single(winners);
            Assert.Equal("Mary", winners.First().Name);
        }

        [Fact]
        public void Two_Players_Have_ThreeKind_Same_Rank()
        {
            // Arrange
            var players = new List<Player>
            {
                DataSeeder.PlayerPaul_2SpadesSuit_JackHigherRank(),
                DataSeeder.PlayerMary_2ClubsSuit_3TenRank(),
                DataSeeder.PlayerDon_3SpadesSuit_3TenRank(),
                DataSeeder.PlayerPeter_2Clubs2DiamondsSuit_3ThreeRank()
            };

            // Action
            var winners = _handEvaluationService.GetWinners(players);

            // Assert
            Assert.Single(winners);
            Assert.Equal("Don", winners.First().Name);
        }
    }
}
