﻿using Poker.Logic.Models;
using System.Collections.Generic;

namespace Poker.Logic.Interfaces
{
    /// <summary>
    /// Provides functions to evaluate winners of poker game
    /// </summary>
    public interface IHandEvaluationService
    {
        /// <summary>
        /// Gets the winners of the showdown
        /// </summary>
        /// <param name="players">Collection of players to evaluate</param>
        /// <returns>Collection of winning players</returns>
        ICollection<Player> GetWinners(ICollection<Player> players);
    }
}
