﻿namespace Poker.Logic.Models
{
    public class Player
    {
        public string Name { get; set; }

        public Card[] Cards
        {
            get
            {
                return _cards;
            }
            set
            {
                if (value.Length != 5)
                {
                    throw new System.ArgumentOutOfRangeException(nameof(value), "The array length must be 5");
                }

                _cards = value;
            }
        }

        private Card[] _cards = new Card[5];
    }
}
