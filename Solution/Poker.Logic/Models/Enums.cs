﻿namespace Poker.Logic.Models
{
    public enum HandRanking
    {
        Flush,
        ThreeKind,
        OnePair,
        HighCard
    }

    public enum Suit
    {
        Clubs,
        Diamonds,
        Hearts,
        Spades
    }

    public enum Rank
    {
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        Ten,
        Jack,
        Queen,
        King,
        Ace
    }
}
