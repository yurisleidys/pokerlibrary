﻿namespace Poker.Logic.Models
{
    public class Card
    {
        public Suit Suit { get; set; }

        public Rank Rank { get; set; }
    }
}
