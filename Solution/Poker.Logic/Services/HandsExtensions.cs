﻿using Poker.Logic.Models;
using System.Collections.Generic;
using System.Linq;

namespace Poker.Logic.Services
{
    /// <summary>
    /// Extensions to evaluate Poker Hands
    /// </summary>
    public static class HandsExtensions
    {
        /// <summary>
        /// Gets if the cards satisfy the rule Flush
        /// </summary>
        /// <param name="cards">Collection of cards</param>
        /// <returns>The rule is or not satisfied</returns>
        public static bool IsFlush(this ICollection<Card> cards)
        {
            var first = cards.First();

            return cards.All(x => x.Suit == first.Suit);
        }

        /// <summary>
        /// Gets if the cards satisfy the rule Three of a Kind
        /// </summary>
        /// <param name="cards">Collection of cards</param>
        /// <returns>The rule is or not satisfied</returns>
        public static bool IsThreeKind(this ICollection<Card> cards)
        {
            foreach (var card in cards)
            {
                if (cards.Count(x => x.Rank == card.Rank) >= 3)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets if the cards satisfy the rule One Pair
        /// </summary>
        /// <param name="cards">Collection of cards</param>
        /// <returns>The rule is or not satisfied</returns>
        public static bool IsOnePair(this ICollection<Card> cards)
        {
            foreach (var card in cards)
            {
                if (cards.Count(x => x.Rank == card.Rank) >= 2)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Sorts the cards of each player by rank
        /// </summary>
        /// <param name="players">Collection of players to sort</param>
        public static void OrderByRankDescending(this ICollection<Player> players)
        {
            foreach (var player in players)
            {
                player.Cards = player.Cards.OrderByDescending(x => (int)x.Rank).ToArray();
            }
        }
    }
}
