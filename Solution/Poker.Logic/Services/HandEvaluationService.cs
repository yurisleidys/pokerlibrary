﻿using Poker.Logic.Interfaces;
using Poker.Logic.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Poker.Logic.Services
{
    /// <summary>
    /// Provides functions to evaluate winners of poker game
    /// </summary>
    public class HandEvaluationService : IHandEvaluationService
    {
        /// <summary>
        /// Gets the winners of the showdown
        /// </summary>
        /// <param name="players">Collection of players to evaluate</param>
        /// <returns>Collection of winning players</returns>
        public ICollection<Player> GetWinners(ICollection<Player> players)
        {
            players.OrderByRankDescending();

            foreach (var ranking in Enum.GetValues(typeof(HandRanking)).Cast<HandRanking>())
            {
                var winners = new List<Player>();

                foreach (var player in players)
                {
                    if (EvaluateHandRanking(ranking, player.Cards))
                    {
                        winners.Add(player);
                    }
                }

                if (winners.Count > 1)
                {
                    return EvaluateTie(winners);
                }
                if (winners.Count > 0)
                {
                    return winners;
                }
            }

            return new List<Player>();
        }

        /// <summary>
        /// Evaluates if player satisfy a rule
        /// </summary>
        /// <param name="evaluator">Rule to evaluate</param>
        /// <param name="playerCards">Collection of cards</param>
        /// <returns>The rule is or not satisfied</returns>
        private bool EvaluateHandRanking(HandRanking evaluator, Card[] playerCards)
        {
            switch (evaluator)
            {
                case HandRanking.Flush:
                    return playerCards.IsFlush();

                case HandRanking.ThreeKind:
                    return playerCards.IsThreeKind();

                case HandRanking.OnePair:
                    return playerCards.IsOnePair();

                case HandRanking.HighCard:
                    return true;

                default:
                    return false;
            }
        }

        /// <summary>
        /// Gets the winners in case of a tie
        /// </summary>
        /// <param name="winners">Collection of winners to evaluate tie</param>
        /// <returns>Collection of final winners</returns>
        private List<Player> EvaluateTie(List<Player> winners)
        {
            int position = 0;

            do
            {
                Rank highCard = Rank.Two;

                foreach (var winner in winners)
                {
                    var value = winner.Cards[position].Rank;

                    if ((int)highCard < (int)value)
                    {
                        highCard = value;
                    }
                }

                winners.RemoveAll(x => x.Cards[position].Rank != highCard);
                position++;
            }
            while (winners.Count > 1 && position < 5);

            return winners;
        }
    }
}
